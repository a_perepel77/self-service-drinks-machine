﻿using self_service_drinks_machine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace self_service_drinks_machine
{
    class Program
    {
        static void Main(string[] args)
        {
            var listDrinks = new List<Drink>();

            var listCategory = new List<Category>();

            listCategory.Add(new Category()
            {
                Name = "soda",
                Id = 1,
                Drinks = new List<Drink>()

            });

            listCategory.Add(new Category()
            {
                Name = "uncarbonated",
                Id = 2,
                Drinks = new List<Drink>()
            });

            var d1 = new Drink();
            d1.Name = "Cola";
            d1.Category = listCategory.FirstOrDefault(a => a.Id == 1);
            d1.CategoryId = listCategory.FirstOrDefault(a => a.Name == "soda").Id;
            d1.Count = 5;
            d1.Price = 70;

            var d2 = new Drink();
            d2.Name = "Sprite";
            d2.Category = listCategory.FirstOrDefault(a => a.Id == 1);
            d2.CategoryId = listCategory.FirstOrDefault(a => a.Name == "soda").Id;
            d2.Count = 7;
            d2.Price = 70;

            var d3 = new Drink();
            d3.Name = "Pepsi";
            d3.Category = listCategory.FirstOrDefault(a => a.Id == 1);
            d3.CategoryId = listCategory.FirstOrDefault(a => a.Name == "soda").Id;
            d3.Count = 3;
            d3.Price = 70;

            var d4 = new Drink();
            d4.Name = "MineralWater";
            d4.Category = listCategory.FirstOrDefault(a => a.Id == 2);
            d4.CategoryId = listCategory.FirstOrDefault(a => a.Name == "uncarbonated").Id;
            d4.Count = 10;
            d4.Price = 40;

            var d5 = new Drink();
            d5.Name = "IceTea";
            d5.Category = listCategory.FirstOrDefault(a => a.Id == 2);
            d5.CategoryId = listCategory.FirstOrDefault(a => a.Name == "uncarbonated").Id;
            d5.Count = 9;
            d5.Price = 100;

            listCategory.FirstOrDefault(a => a.Name != "uncarbonated").Drinks.Add(d1);
            listCategory.FirstOrDefault(a => a.Name != "uncarbonated").Drinks.Add(d2);
            listCategory.FirstOrDefault(a => a.Name != "uncarbonated").Drinks.Add(d3);
            listCategory.FirstOrDefault(a => a.Name != "soda").Drinks.Add(d4);
            listCategory.FirstOrDefault(a => a.Name != "soda").Drinks.Add(d5);

            listDrinks.Add(d1);
            listDrinks.Add(d2);
            listDrinks.Add(d3);
            listDrinks.Add(d4);
            listDrinks.Add(d5);

            var Print = listDrinks.Where(a => a.CategoryId ==2).Where(a=>a.Price>50).ToList();

            foreach (var element in Print)
            {
                Console.WriteLine($"Из негазированных напитков дороже 50 рублей, мы можем предложить вам {element.Name} за {element.Price} рублей");
                Console.ReadLine();
            }

           






        }
    }
}
