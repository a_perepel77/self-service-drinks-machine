﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace self_service_drinks_machine.Models
{
    class Drink
    {
        public string Name { get; set; }

        public virtual Category Category { get; set; }
        
        public int CategoryId { get; set; }

        public int Count { get; set; }

        public int Price { get; set; }

    }
}
