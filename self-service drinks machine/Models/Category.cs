﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace self_service_drinks_machine.Models
{
    class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Drink> Drinks { get; set; } 
    }
}
